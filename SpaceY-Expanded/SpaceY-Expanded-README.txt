SpaceY Expanded - Part Pack

by Ed T. Toton III, aka NecroBones.


Installation:

Simply copy the "SpaceY-Expanded" folder into your GameData folder. If upgrading from a 
previous version, be sure to delete the old one from GameData first (this is the cleanest
option).

Requires ModuleManager and SpaceY-Lifters as dependencies. 


Information:

This parts pack is an expansion for the SpaceY Heavy Lifters pack, and requires 
that one as a dependency. 

The name is a play on "SpaceX" in the real world, but also has the amusing double
connotations of being "spacey", and also "Space, why?" -- Because it's there!

We hope you enjoy this pack. If you have any comments or feedback, please feel free to
send a private message to NecroBones on the KSP forums. Thanks!


Forum Thread:
http://forum.kerbalspaceprogram.com/threads/133301


KerbalStuff download:
https://kerbalstuff.com/mod/1137/SpaceY Expanded


This parts pack is being shared under the CC-BY-NC-SA license:
http://creativecommons.org/licenses/by-nc-sa/4.0/

