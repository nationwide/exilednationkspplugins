# README #

KSP Mod List

### Fix this shit ###
* List of Mods + link:
### Part Mods ### ### Part Mods ### ### Part Mods ###
* [x]Science 						v5.17 		(Aug 10th 2018) 		 : http://goo.gl/XgB0qA

### Graphical/System Changes Mods ###
* AlternisKerbolRekerjiggered 		v1.7 		(Nov 13th 2016) 	 : http://goo.gl/Ra6Xu3


* B9 Aerospace						v6.2 pre  	(Oct 28th 2016) 	 : http://goo.gl/b2X8uf
* B9PartSwitch						v1.5.3 		(Dec 8th 2016)  	 : http://goo.gl/Z6Tdjy
* BahaSP (B.Dynamics)				v1.2.0		(May 6th 2016) 		 : http://goo.gl/yiTuyJ
* BodyLoader (realistic atmo)		v1.2.2 		(July 28th 2016)	 : http://goo.gl/5TVVl2
* Canadarm							v1.71 		(June 5th 2016) 	 : http://goo.gl/FmOs7p (removed)
* Community Resource Pack			v6.3.0		(Nov 15th 2016) 	 : http://goo.gl/W6mKJJ
* Community Tech Tree				v3.0.2		(Nov 10th 2016) 	 : http://goo.gl/1z4ho9
* Connected Living Spaces			v1.2.1.5	(June 13th 2016)	 : http://goo.gl/fIWrNV (being updated)
* CryoEngines (Tanks,deployable)	v0.4.3 		(Nov 18th 2016) 	 : http://goo.gl/tMabW7
* CxAerospace						v1.4		(Oct 12th 2016)		 : http://goo.gl/B7Dj0d
* Deadly Re-entry					v7.4.5 		(May 31st 2016)		 : http://goo.gl/bGnVwH (being updated)
* DMagicOrbitalScience				v1.3.7 		(Dec 13th 2016) 	 : http://goo.gl/hYUYvL
* Engine Lighting 					v1.5 		(Nov 19th 2016)		 : http://goo.gl/vbGhqz -graphical
* EnvironmentalVisualEnhancements (DO NOT USE CONFIG)(Oct 13th 2016) : http://goo.gl/qss55u	-graphical | http://goo.gl/f17FyF
* Eskandere (thermonuclear Turbines)v1.0.12		(May 10th 2016)		 : http://goo.gl/VCZ3V0 (not updated, still works?)
* FarramAerospaceResearch (FAR)		v0.15.7.2	(July 1st 2016)		 : http://goo.gl/Zl7i5I -includes ModularFlightIntegrator (updated?)
* Firespitter (dependency only)		v7.4.2		(Dec 15th 2016)		 : http://goo.gl/ZFlM0G -(needs updating)
* FogofTech							v0.4		(Oct 12th 2016)		 : http://goo.gl/lFq5J5 -technically, graphical
* HabTech							v0.1.7 		(May 2nd 2016) 		 : http://goo.gl/VPN4BM (removed, being updated)
* HideEmptyTreeNodes				v0.7		(Dec 2nd 2016)		 : http://goo.gl/c4dNnW -technically, graphical
* Interstellar Fuel Switch			v2.3		(Dec 9th 2016)		 : http://goo.gl/9TSwCO
* JSI (RasterPropMonitor)			v0.28.0 	(Nov 10th 2016)	     : http://goo.gl/hHWE20
* KAS Kerbal Attachment System		v0.6.1 		(Dec 6th 2016)	     : http://goo.gl/M0QszF
* KASE								v0.2 		(May 4th 2016) 		 : http://goo.gl/Ldofui (removed, not working?)
* Kerbal Engineer Redux				v1.1.2.8 	(Dec 2nd 2016)		 : http://goo.gl/tW2szL
* KerbalHacks (Asphalt)				v1.2 		(April 29th 2016) 	 : http://goo.gl/PpJULV (still works maybe?)
* Kerbalism 						v1.1.5 		(Dec 14th 2016)		 : http://goo.gl/IklH5V (pre-release)
* Kerbal Joint Reinforcement		v3.3.1 		(Oct 29th 2016)		 : http://goo.gl/zEmxr1
* KermangeddonIndustries (Ballutes) v1.2.7 		(Oct 25th 2016) 	 : http://goo.gl/QR2YBQ
* Kethane							v0.9.7		(Oct 28th 2016)		 : http://goo.gl/usqJzs
* KIS Kerbal Inventory System 		v1.4	 	(Dec 4th 2016)	 	 : http://goo.gl/XkztY8
* Kopernicus 						v1.2.2-2	(Dec 7th 2016)	 	 : http://goo.gl/bSoGIh
* LETech (Litho Explor Tech)		v0.4 		(Oct 12th 2016)		 : http://goo.gl/Iq6Rkp
* MagicSmokeIndustries (Robots)		v2.0.10		(Dec 15th 2016)  	 : http://goo.gl/4Kn3sF (being updated)
* MechJeb2							v2.6.0 		(Dec 12th 2016)		 : http://goo.gl/miwJym
* Mk2Expansion						v1.7.25		(Nov 12th 2016)		 : http://goo.gl/r7QmXF
* NavyFish (DockingPortIndicator) 	v6.5.1		(Oct 25th 2016) 	 : http://goo.gl/H0W1KD
* Near Future Construction			v0.7.2 	 	(Nov 18th 2016)		 : http://goo.gl/hnnLvc -includes B9PartSwitch
* Near Future Electrical 			v0.8.2 		(Nov 18th 2016)  	 : Same
* Near Future Propulsion			v0.8.2		(Nov 18th 2016)		 : Same
* Near Future Solar					v0.7.1 		(Nov 18th 2016)    	 : Same
* Near Future Spacecraft			v0.5.4 		(Nov 18th 2016) 	 : Same					-includes NearFutureProps
* OPM (Outer planets mod)			v2.1 		(Nov 17st 2016)		 : http://goo.gl/CCwMgg -includes CTTP
* OPT (Space Plane)					v1.0.3.5	(Dec 9th 2016)   	 : http://goo.gl/Fp2YVI -UpdateURL: http://goo.gl/7ANzNH
* Other Worlds						v1.0.3 		(April 3rd 2016) 	 : http://goo.gl/3h5Jy5 (Still works?)
* PlanetaryBaseInc					v1.3.9 		(Dec 9th 2016)	 	 : http://goo.gl/1WQcZX
* Planetshine						v0.2.5.2	(Oct 4th 2016)  	 : http://goo.gl/vDKE2v -graphical
* ProceduralDynamics (Wings)		v0.12.0 	(Oct 14th 2016) 	 : http://goo.gl/EHm4B5
* RealChute							v1.4.1.2	(Oct 14th 2016) 	 : http://goo.gl/GO7cL7
* RealPlume							v0.11.1 	(Nov 20th 2016) 	 : http://goo.gl/E7jrJT
* REPOSoftTech (Deep Freeze)		v0.23.2 	(Dec 8th 2016)	 	 : http://goo.gl/czcD3X
* RemoteTech 						v1.8.3 		(Dec 10th 2016)  	 : http://goo.gl/D4L2Pb
* SCANsat							v16.11 		(Nov 4th 2016)		 : http://goo.gl/yX18TK
* Scatterer							v0.0256 	(Nov 17th 2016) 	 : http://goo.gl/SoQs6I -graphical
* SpaceY - Expanded 				v1.3.1		(Nov 3rd 2016)  	 : http://goo.gl/u7GcWT
* SpaceY - Lifters					v1.15	 	(Oct 21st 2016) 	 : http://goo.gl/BpLaUI
* StationSCience					v2.0 		(Oct 17th 2016) 	 : http://goo.gl/ACVpeF (being updated)
* Stock Visual Enhancements			v1.1.4 		(Oct 22nd 2016) 	 : http://goo.gl/z52EDw -graphical
* SXMk2 (SXT)						v0.3.5 		(Dec 13th 2016) 	 : http://goo.gl/MJT4u2
* Telemachus						v2.0 		(June 3rd 2016) 	 : http://spacedock.info/mod/742/Telemachus%202.0 (removed)
* TweakScale						v2.3.2	    (Nov 3rd 2016)	 	 : http://goo.gl/hkoCtM
* WarpPlugin (KSPI)					v1.11.8 	(Dec 14th 2016) 	 : http://goo.gl/vzGrfy -includes RcsSounds 000_ folders


### Mods to watch ###
* Better Atmospheres http://forum.kerbalspaceprogram.com/index.php?/topic/70221-025090-better-atmospheres-v5-june-14th-2014/
* http://forum.kerbalspaceprogram.com/index.php?/topic/51142-1121-1-2-may-13-2016-environmentalvisualenhancements/&do=findComment&comment=2587261
* New Horizons: http://forum.kerbalspaceprogram.com/index.php?/topic/102776-105-kopernicus-new-horizons-v17-19nov15-back-in-business/
* http://forum.kerbalspaceprogram.com/index.php?/topic/139128-112-memgraph-1007-with-stutter-reduction/
* http://forum.kerbalspaceprogram.com/index.php?/topic/91713-105-mark-iv-spaceplane-system-the-go-faster-update-minor-hotfix-030116/&page=1
* http://forum.kerbalspaceprogram.com/index.php?/topic/69702-112-ksprc-renaissance-compilation-artworks-remake-v-07-pre-release-3/&page=1


### How do I get set up? ###

* You ask nationwide who gives you horrible abbreviated instructions assuming you know rocket science.
* False, you ask nationwide via some medium that is not text, so that he doesnt have to type on a tiny fucking keyboard
* But if you do ask via text, expect slightly abbreviated instructions, but thourough nonetheless, simple enough that a monkey can figure it out
* Just ask Twisty, he figured it out

### Then How do I really get set up? ###

* You don't
* Google
* Silently struggle with the realization that you can't play KSP yet because Git has to update
* Or you know, just clone the repo into your gamedata folder
* Its really hard

### Contribution guidelines ###

* Contributions? What?
* Add Kerbalism and Infernal Robotics
* ^Them be my contribs
* Contact Twisty, he be the Queen of the mods

### Who do I talk to? ###

* Nationwide
* cause
* hes ON YOUR SIDE
* He's not wrong